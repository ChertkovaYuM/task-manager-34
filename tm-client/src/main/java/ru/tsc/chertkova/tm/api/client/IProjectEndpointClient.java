package ru.tsc.chertkova.tm.api.client;

import ru.tsc.chertkova.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
