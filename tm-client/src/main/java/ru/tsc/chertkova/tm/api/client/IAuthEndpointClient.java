package ru.tsc.chertkova.tm.api.client;

import ru.tsc.chertkova.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {
}
