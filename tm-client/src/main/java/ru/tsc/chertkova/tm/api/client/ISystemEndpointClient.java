package ru.tsc.chertkova.tm.api.client;

import ru.tsc.chertkova.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {
}
