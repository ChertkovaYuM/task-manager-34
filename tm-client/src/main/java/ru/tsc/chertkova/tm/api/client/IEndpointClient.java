package ru.tsc.chertkova.tm.api.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IEndpointClient {

    @NotNull
    @SneakyThrows
    Socket connect();

    @NotNull
    @SneakyThrows
    Socket disconnect();

    @NotNull
    Socket getSocket();

    void setSocket(@Nullable Socket socket);

}
