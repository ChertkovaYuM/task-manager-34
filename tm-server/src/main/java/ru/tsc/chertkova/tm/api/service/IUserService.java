package ru.tsc.chertkova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @Nullable
    User setPassword(@Nullable String userId, @Nullable String password);

    @Nullable
    User updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    User lockUserByLogin(@Nullable String login);

    User unlockUserByLogin(@Nullable String login);

}
