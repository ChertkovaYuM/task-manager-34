package ru.tsc.chertkova.tm.api.endpoint;

import ru.tsc.chertkova.tm.dto.request.AbstractRequest;
import ru.tsc.chertkova.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
