package ru.tsc.chertkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(@Nullable String token) {
        super(token);
    }

}
